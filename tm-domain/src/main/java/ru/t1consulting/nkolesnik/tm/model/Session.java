package ru.t1consulting.nkolesnik.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Session extends AbstractWbsModel {

    @NotNull
    private Date date = new Date();

    @Nullable
    private Role role = null;

}
