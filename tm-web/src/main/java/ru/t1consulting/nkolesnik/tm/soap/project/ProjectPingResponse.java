package ru.t1consulting.nkolesnik.tm.soap.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "success",
        "message"
})
@XmlRootElement(name = "ProjectPingResponse")
public class ProjectPingResponse {

    protected boolean success;

    @XmlElement(required = true)
    protected String message;

    public boolean isSuccess() {
        return success;
    }

}
