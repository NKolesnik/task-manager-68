package ru.t1consulting.nkolesnik.tm.config;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;
import ru.t1consulting.nkolesnik.tm.model.dto.AbstractModelDto;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.t1consulting.nkolesnik.tm")
@PropertySource("classpath:application.properties")
@EnableJpaRepositories("ru.t1consulting.nkolesnik.tm.api.repository.dto")
public class ApplicationConfiguration {

    @NotNull
    private static final String HAZELCAST_USE_LITE_MEMBER = "hibernate.cache.hazelcast.use_lite_member";

    @NotNull
    private static final String PROJECT_NAMESPACE = "http://t1consulting.ru/nkolesnik/tm/soap/project";

    @NotNull
    private static final String PROJECT_PORT_TYPE = "/ProjectSoapEndpointPort";

    @NotNull
    private static final String TASK_NAMESPACE = "http://t1consulting.ru/nkolesnik/tm/soap/project";

    @NotNull
    private static final String TASK_PORT_TYPE = "/TaskSoapEndpointPort";

    @NotNull
    private static final String LOCATION_URI = "/ws";


    @Bean
    @NotNull
    public DataSource dataSource(
            @NotNull @Value("#{environment['database.driver']}") final String databaseDriver,
            @NotNull @Value("#{environment['database.url']}") final String databaseConnectionString,
            @NotNull @Value("#{environment['database.username']}") final String databaseUser,
            @NotNull @Value("#{environment['database.password']}") final String databasePassword
    ) {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(databaseDriver);
        dataSource.setUrl(databaseConnectionString);
        dataSource.setUsername(databaseUser);
        dataSource.setPassword(databasePassword);
        return dataSource;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NotNull final DataSource dataSource,
            @NotNull @Value("#{environment['database.dialect']}") final String dialect,
            @NotNull @Value("#{environment['database.hbm2ddl_auto']}") final String hbm2Ddl,
            @NotNull @Value("#{environment['database.show_sql']}") final String showSql,
            @NotNull @Value("#{environment['database.format_sql']}") final String formatSql,
            @NotNull @Value("#{environment['cache.use_second_lvl_cache']}") final String useSecondLvlCache,
            @NotNull @Value("#{environment['cache.provider_config_file']}") final String cacheProviderConfigFile,
            @NotNull @Value("#{environment['cache.region.factory_class']}") final String cacheRegionFactoryClass,
            @NotNull @Value("#{environment['cache.use_query_cache']}") final String cacheUseQueryCache,
            @NotNull @Value("#{environment['cache.use_min_puts']}") final String cacheUseMinPuts,
            @NotNull @Value("#{environment['cache.region_prefix']}") final String cacheRegionPrefix,
            @NotNull @Value("#{environment['cache.hazelcast.use_lite_member']}") final String cacheHazelcastUseLiteMember
    ) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan(
                AbstractModelDto.class.getPackage().getName()
        );
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DIALECT, dialect);
        properties.put(Environment.HBM2DDL_AUTO, hbm2Ddl);
        properties.put(Environment.SHOW_SQL, showSql);
        properties.put(Environment.FORMAT_SQL, formatSql);
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, useSecondLvlCache);
        properties.put(Environment.CACHE_PROVIDER_CONFIG, cacheProviderConfigFile);
        properties.put(Environment.CACHE_REGION_FACTORY, cacheRegionFactoryClass);
        properties.put(Environment.USE_QUERY_CACHE, cacheUseQueryCache);
        properties.put(Environment.USE_MINIMAL_PUTS, cacheUseMinPuts);
        properties.put(Environment.CACHE_REGION_PREFIX, cacheRegionPrefix);
        properties.put(HAZELCAST_USE_LITE_MEMBER, cacheHazelcastUseLiteMember);
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean(name = "ProjectEndpoint")
    public DefaultWsdl11Definition projectWsdl11Definition(@NotNull final XsdSchema projectEndpointSchema) {
        @NotNull final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(PROJECT_PORT_TYPE);
        wsdl11Definition.setLocationUri(LOCATION_URI);
        wsdl11Definition.setTargetNamespace(PROJECT_NAMESPACE);
        wsdl11Definition.setSchema(projectEndpointSchema);
        return wsdl11Definition;
    }

    @Bean(name = "TaskEndpoint")
    public DefaultWsdl11Definition taskWsdl11Definition(@NotNull final XsdSchema taskEndpointSchema) {
        @NotNull final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(TASK_PORT_TYPE);
        wsdl11Definition.setLocationUri(LOCATION_URI);
        wsdl11Definition.setTargetNamespace(TASK_NAMESPACE);
        wsdl11Definition.setSchema(taskEndpointSchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema projectEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/projectEndpoint.xsd"));
    }

    @Bean
    public XsdSchema taskEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/taskEndpoint.xsd"));
    }

}
