package ru.t1consulting.nkolesnik.tm.soap.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1consulting.nkolesnik.tm.model.dto.ProjectDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@Getter
@Setter
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "project"
})
@XmlRootElement(name = "ProjectFindByIdResponse")
public class ProjectFindByIdResponse {

    protected ProjectDto project;

}
