package ru.t1consulting.nkolesnik.tm.soap.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.*;


@Getter
@Setter
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "id"
})
@XmlRootElement(name = "TaskExistsByIdRequest")
public class TaskExistsByIdRequest {

    @XmlElement(required = true)
    protected String id;

}
