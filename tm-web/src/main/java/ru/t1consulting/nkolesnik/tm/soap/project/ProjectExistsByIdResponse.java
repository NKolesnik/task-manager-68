package ru.t1consulting.nkolesnik.tm.soap.project;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "exists"
})
@XmlRootElement(name = "ProjectExistsByIdResponse")
public class ProjectExistsByIdResponse {

    protected boolean exists;

}
