package ru.t1consulting.nkolesnik.tm.soap.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "id"
})
@XmlRootElement(name = "ProjectExistsByIdRequest")
public class ProjectExistsByIdRequest {

    @XmlElement(required = true)
    protected String id;

}
