package ru.t1consulting.nkolesnik.tm.api.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import ru.t1consulting.nkolesnik.tm.soap.project.*;

public interface IProjectDtoSoapEndpoint {

    @NotNull
    String NAMESPACE = "http://t1consulting.ru/nkolesnik/tm/soap/project";

    @PayloadRoot(localPart = "ProjectCreateRequest", namespace = NAMESPACE)
    ProjectCreateResponse create(@NotNull @RequestPayload ProjectCreateRequest request);

    @PayloadRoot(localPart = "ProjectExistsByIdRequest", namespace = NAMESPACE)
    ProjectExistsByIdResponse existsById(@NotNull @RequestPayload ProjectExistsByIdRequest request);

    @PayloadRoot(localPart = "ProjectFindByIdRequest", namespace = NAMESPACE)
    ProjectFindByIdResponse findById(@NotNull @RequestPayload ProjectFindByIdRequest request);

    @PayloadRoot(localPart = "ProjectFindAllRequest", namespace = NAMESPACE)
    ProjectFindAllResponse findAll(@NotNull @RequestPayload ProjectFindAllRequest request);

    @PayloadRoot(localPart = "ProjectCountRequest", namespace = NAMESPACE)
    ProjectCountResponse count(@NotNull @RequestPayload ProjectCountRequest request);

    @PayloadRoot(localPart = "ProjectSaveRequest", namespace = NAMESPACE)
    ProjectSaveResponse save(@NotNull @RequestPayload ProjectSaveRequest request);

    @PayloadRoot(localPart = "ProjectDeleteRequest", namespace = NAMESPACE)
    ProjectDeleteResponse delete(@NotNull @RequestPayload ProjectDeleteRequest request);

    @PayloadRoot(localPart = "ProjectDeleteByIdRequest", namespace = NAMESPACE)
    ProjectDeleteByIdResponse deleteById(@NotNull @RequestPayload ProjectDeleteByIdRequest request);

    @PayloadRoot(localPart = "ProjectDeleteAllRequest", namespace = NAMESPACE)
    ProjectDeleteAllResponse deleteAll(@NotNull @RequestPayload ProjectDeleteAllRequest request);

    @PayloadRoot(localPart = "ProjectClearRequest", namespace = NAMESPACE)
    ProjectClearResponse clear(@NotNull @RequestPayload ProjectClearRequest request);

    @PayloadRoot(localPart = "ProjectPingRequest", namespace = NAMESPACE)
    ProjectPingResponse ping(@NotNull @RequestPayload ProjectPingRequest request);

}
