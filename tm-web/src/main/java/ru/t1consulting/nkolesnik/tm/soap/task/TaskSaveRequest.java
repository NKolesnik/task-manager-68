package ru.t1consulting.nkolesnik.tm.soap.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1consulting.nkolesnik.tm.model.dto.TaskDto;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "task"
})
@XmlRootElement(name = "TaskSaveRequest")
public class TaskSaveRequest {

    @XmlElement(required = true)
    protected TaskDto task;

}
