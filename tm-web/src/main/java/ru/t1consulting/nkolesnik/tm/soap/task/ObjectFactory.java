package ru.t1consulting.nkolesnik.tm.soap.task;

import ru.t1consulting.nkolesnik.tm.model.dto.TaskDto;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {

    public ObjectFactory() {
    }

    public TaskCreateRequest createTaskCreateRequest() {
        return new TaskCreateRequest();
    }

    public TaskCreateResponse createTaskCreateResponse() {
        return new TaskCreateResponse();
    }

    public TaskExistsByIdRequest createTaskExistsByIdRequest() {
        return new TaskExistsByIdRequest();
    }

    public TaskExistsByIdResponse createTaskExistsByIdResponse() {
        return new TaskExistsByIdResponse();
    }

    public TaskFindByIdRequest createTaskFindByIdRequest() {
        return new TaskFindByIdRequest();
    }

    public TaskFindByIdResponse createTaskFindByIdResponse() {
        return new TaskFindByIdResponse();
    }

    public TaskDto createTaskDto() {
        return new TaskDto();
    }

    public TaskFindAllRequest createTaskFindAllRequest() {
        return new TaskFindAllRequest();
    }

    public TaskFindAllResponse createTaskFindAllResponse() {
        return new TaskFindAllResponse();
    }

    public TaskCountRequest createTaskCountRequest() {
        return new TaskCountRequest();
    }

    public TaskCountResponse createTaskCountResponse() {
        return new TaskCountResponse();
    }

    public TaskSaveRequest createTaskSaveRequest() {
        return new TaskSaveRequest();
    }

    public TaskSaveResponse createTaskSaveResponse() {
        return new TaskSaveResponse();
    }

    public TaskDeleteRequest createTaskDeleteRequest() {
        return new TaskDeleteRequest();
    }

    public TaskDeleteResponse createTaskDeleteResponse() {
        return new TaskDeleteResponse();
    }

    public TaskDeleteByIdRequest createTaskDeleteByIdRequest() {
        return new TaskDeleteByIdRequest();
    }

    public TaskDeleteByIdResponse createTaskDeleteByIdResponse() {
        return new TaskDeleteByIdResponse();
    }

    public TaskDeleteAllRequest createTaskDeleteAllRequest() {
        return new TaskDeleteAllRequest();
    }

    public TaskDeleteAllResponse createTaskDeleteAllResponse() {
        return new TaskDeleteAllResponse();
    }

    public TaskClearRequest createTaskClearRequest() {
        return new TaskClearRequest();
    }

    public TaskClearResponse createTaskClearResponse() {
        return new TaskClearResponse();
    }

    public TaskPingRequest createTaskPingRequest() {
        return new TaskPingRequest();
    }

    public TaskPingResponse createTaskPingResponse() {
        return new TaskPingResponse();
    }

}
