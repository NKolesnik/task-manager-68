package ru.t1consulting.nkolesnik.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1consulting.nkolesnik.tm.model.dto.TaskDto;

import java.util.Collection;
import java.util.List;

@Service
public class TaskDtoService {

    @NotNull
    @Autowired
    private ITaskDtoRepository taskRepository;

    public void add(@NotNull final TaskDto task) {
        taskRepository.saveAndFlush(task);
    }

    public void create() {
        @NotNull final TaskDto task = new TaskDto();
        task.setName("New Task" + System.currentTimeMillis());
        taskRepository.saveAndFlush(task);
    }

    public boolean existsById(@NotNull final String id) {
        return taskRepository.existsById(id);
    }

    @Nullable
    public TaskDto findById(@NotNull final String id) {
        return taskRepository.findById(id).orElse(null);
    }

    @Nullable
    public Collection<TaskDto> findAll() {
        return taskRepository.findAll();
    }

    public long count() {
        return taskRepository.count();
    }

    public void save(@NotNull final TaskDto task) {
        taskRepository.saveAndFlush(task);
    }

    public void deleteById(@NotNull final String id) {
        taskRepository.deleteById(id);
    }

    public void delete(@NotNull final TaskDto task) {
        taskRepository.delete(task);
    }

    public void deleteAll(@NotNull final List<TaskDto> taskList) {
        taskRepository.deleteAll(taskList);
    }

    public void clear() {
        taskRepository.deleteAll();
    }

}
