package ru.t1consulting.nkolesnik.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.ws.transport.http.MessageDispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

@EnableWebMvc
@ComponentScan("ru.t1consulting.nkolesnik.tm")
public class WebApplicationConfiguration implements ApplicationContextAware, ServletContextAware {

    @NotNull
    private ApplicationContext applicationContext;

    @NotNull
    private ServletContext servletContext;

    @Override
    public void setApplicationContext(@NotNull final ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public void setServletContext(@NotNull final ServletContext servletContext) {
        this.servletContext = servletContext;
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        @NotNull final ServletRegistration.Dynamic dispatcher = servletContext.addServlet(
                "message-dispatcher",
                servlet
        );
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping("/ws/*");
    }


    @Bean
    public ViewResolver internalResourceViewResolver() {
        @NotNull final InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setViewClass(JstlView.class);
        resolver.setPrefix("/WEB-INF/views/");
        resolver.setSuffix(".jsp");
        return resolver;
    }

}
