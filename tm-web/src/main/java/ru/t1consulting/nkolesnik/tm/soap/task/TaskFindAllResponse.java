package ru.t1consulting.nkolesnik.tm.soap.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1consulting.nkolesnik.tm.model.dto.TaskDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "task"
})
@XmlRootElement(name = "TaskFindAllResponse")
public class TaskFindAllResponse {

    protected List<TaskDto> task;

}
