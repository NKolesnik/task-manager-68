package ru.t1consulting.nkolesnik.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.dto.request.system.ServerAboutRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.system.ServerAboutResponse;
import ru.t1consulting.nkolesnik.tm.event.ConsoleEvent;

@Component
public final class AboutListener extends AbstractSystemListener {

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String DESCRIPTION = "Show developer info.";

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @EventListener(condition = "@aboutListener.getName() == #event.name || @aboutListener.getArgument() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[ABOUT]");
        ServerAboutResponse response = getSystemEndpoint().getAbout(new ServerAboutRequest());
        System.out.println("Name: " + response.getName());
        System.out.println("E-mail: " + response.getEmail());
    }

}
