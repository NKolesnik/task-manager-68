package ru.t1consulting.nkolesnik.tm.service.model;

import org.springframework.stereotype.Service;
import ru.t1consulting.nkolesnik.tm.api.service.model.IUserOwnedService;
import ru.t1consulting.nkolesnik.tm.model.AbstractUserOwnedModel;

@Service
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel>
        extends AbstractService<M> implements IUserOwnedService<M> {

}
